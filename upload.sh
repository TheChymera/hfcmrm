ARTICLE_NAME="hfcmrm"
SERVER='dreamarticles'
WEBSITE='articles.chymera.eu'
[[ ! -z "$HOSTNAME" ]] && HOSTNAME="${HOSTNAME}_"
rsync -avP main.pdf ${SERVER}:${WEBSITE}/${HOSTNAME}${ARTICLE_NAME}.pdf &&\
                echo "Article uploaded to http://${WEBSITE}/${HOSTNAME}${ARTICLE_NAME}.pdf"
rsync -avP review.pdf ${SERVER}:${WEBSITE}/${HOSTNAME}${ARTICLE_NAME}_review.pdf &&\
                echo "Article uploaded to http://${WEBSITE}/${HOSTNAME}${ARTICLE_NAME}_review.pdf"
