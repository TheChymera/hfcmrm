#!/usr/bin/env bash

rm submission_format -rf || true
mkdir submission_format

arlatex --outfile=submission_format/allinone.tex --document=main.tex *tex

cp img/*{pdf,png} submission_format
cp *{bst,cls,bib} submission_format
cp compile.sh submission_format

pushd submission_format
	ag "img/" -l0 | xargs -0 sed -i -e "s:img/::g"
	mkdir document_generation
	pushd document_generation
		cp ../* .
		./compile.sh allinone
		mv allinone.pdf ../
	popd
	rm document_generation -rf 
popd

rm submission_format.zip
zip -r submission_format.zip submission_format
