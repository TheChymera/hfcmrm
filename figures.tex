\begin{figure} [ht]
	\begin{center}
		\begin{tabular}{c}
			\includegraphics[width=.7\textwidth]{img/fpsetup.png}
		\end{tabular}
	\end{center}
	\caption[example]{
		\label{fig:fpsetup}
		\textbf{Fiber photometry is based on an advanced yet robust multi-component system leveraging optical fiber light transmission in order to separate MR and optical instrumentation:}
		A laser beam is coupled into a fiber-optic patch cable that is connected to an implant on the mouse;
		fluorescent emissions are guided back through the same fiber;
		the entire optical setup is located outside of the MR scanner room.
		MRI volumes, the fluorescence time course, and the stimulation protocol are then combined in offline analysis.
		Abbreviations:
			PMT (photomultiplier tube),
			EF (emission filter),
			DM (dichroic mirror),
			CL (coupling lens),
			FP (fiber port),
			ND (neutral density),
			DAQ (data acquisition device).
		Adapted from \cite{Schlegel2018}.
	}
\end{figure}

\begin{sidewaystable}
	\renewcommand{\arraystretch}{1.1}
	\let\footnoterule\relax
	\footnotesize
	\centering
	\caption{
		\label{tab:recording}
		\textbf{Studies using hybrid systems for optical Ca\textsuperscript{2+}-recording and fMRI in-vivo.}
		Abbreviations:
			ASL (arterial spin labeling),
			fp. (forepaw),
			FP (fiber photometry),
			hp. (hindpaw),
			iso. (isoflurane),
			LGN (lateral geniculate nucleus),
			med. (medetomidine),
			Po (posterior thalamic nucleus),
			RS (resting state),
			S1 (primary somateosensory cortex),
			SC (superior colliculus),
			stim. (stimulation),
			s.v. (single-vessel),
			ur. (urethane),
			WI (widefield imaging).
		}
	\vspace{.8em}
	%\begin{tabular}{lcccccccc}\toprule
	\begin{tabular}{lllllllr}\toprule
		Author, Year		& Optical Method& fMRI Method		& Target Cells 				& Brain Region & Paradigm & Anesthesia & Species \\ \midrule
		Schulz et al. 2012\cite{Schulz2012} 	& FP 		& BOLD 			& unspecific Ca\textsuperscript{2+} 	& fp./hp. S1 & fp./hp. stim. & iso. & rat  \\
		Liang et al. 2017\cite{Liang2017} 	& FP		& BOLD 			& neurons 				& SC & visual stim. & med. & rat \\
		Schwalm et al. 2017\cite{Schwalm2017} 	& FP 		& BOLD 			& neurons 				& fp. S1, Po & RS & iso. & rat \\
		He et al. 2018\cite{He2018} 		& FP 		& BOLD, CBV (s.v.)	& neurons 				& fp. S1, vibrissa S1 & fp., RS & $\alpha$-chloralose & rat \\
		Schlegel et al. 2018\cite{Schlegel2018} 	& FP 		& BOLD 			& neurons, astrocytes 			& hp. S1 & hp. stim., RS & iso. & mouse \\
		Wang et al. 2018\cite{Wang2018} 	& FP 		& BOLD			& astrocytes				& fp. S1 & RS & ur. & rat \\
					& FP 		& BOLD			& astrocytes 				& fp. S1 & fp. stim. & med. & rat \\
					& FP 		& BOLD			& neurons, astrocytes 			& fp. S1 & fp. stim., RS& $\alpha$-chloralose & rat \\
		Chen et al. 2019\cite{Chen2019} 	& FP 		& BOLD 			& neurons 				& barrel S1 & optogenetic stim. & $\alpha$-chloralose & rat \\
		Tong et al. 2019\cite{Tong2019} 	& FP\footnote{within-subject 2-site optical measurement} 	& BOLD 			& neurons 				& SC, LGN & visual stim., RS & med. & rat \\
		Van Alst et al. 2019\cite{Alst2019} 	& FP 		& BOLD, ASL 		& neurons 				& fp. S1 & fp. stim. & iso.-med. & rat \\
		Lake et al. 2020\cite{Lake2020} 	& WI 		& BOLD 			& excitatory neurons 			& whole cortex & hp. stim. & iso. & mouse \\
		Pais-Roldan et al. 2020\cite{PaisRoldn2020} & FP 		& BOLD 			& neurons 				& cingulate cortex & pupil size correlated RS & $\alpha$-chloralose & rat \\ \bottomrule
	\end{tabular}
\end{sidewaystable}

\begin{sidewaystable}
	\renewcommand{\arraystretch}{1.1}
	\let\footnoterule\relax
	\scriptsize
	\centering
	\caption{
		\scriptsize
		\label{tab:stim}
		\textbf{Literature overview of opto-fMRI experiments.}
		All optogenetic constructs are excitatory and have rapid kinetics, unless otherwise noted.
		Cre-LoxP selection for expression is prepended in parentheses to the promoter, when applicable.
		If the experiment uses vector injection as opposed to constitutive optogenetic construct expression and the injection site differs from the stimulation site, the injection site is prepended in parentheses.
		Paradigms detail in parentheses the characteristics of the smallest structured stimulation train, and original sources should be consulted, as ON periods may contain additional pauses between stimulation trains.
		Step function opsins do not possess canonical within-stimulation-period parameters.
		Abbreviations:
			C1V1 (Chlamydomonas reinhardtii and Volvox carteri chimeric channelrhodopsin, with E122T and E162T mutations),
			CA1 (hippocampus cornu ammonis 1),
			CAG (cytalomegavirus early enhancer/chicken $\beta$-actin),
			CaMKII (calcium-calmodulin kinase II$\alpha$),
			CC (corpus callosum),
			CHRM4 (cholinergic receptor suscarinic 4),
			dfMRI (diffusion fMRI),
			DH (dorsal hippocampus)
			DR (dorsal raphe),
			EF1$\alpha$ (human elongation factor 1 alpha),
			eNpHR3.0 (Chlamydomonas reinhardtii halorhodopsin variant),
			ePET (Pet-1 enhancer),
			ER (event-related),
			FEF (frontal eyefield),
			$f_{rep}$ (laser pulse repetition frequency),
			hChR2 (humanized Channelrhodopsin 2, variant in parentheses),
			hSyn (human synapsin 1 promoter),
			IH (intermediate hippocampus),
			IL mPFC (infralimbic medial prefrontal cortex),
			LH (lateral hypothalamus),
			Mlc1 (myosin alkali light chains),
			MVN (medial vestibular nucleus),
			NAcc (nucleus accumbens),
			PFC (prefrontal cortex),
			PV (parvalbumin promoter),
			rhesus m. (rhesus macaque),
			SSFO (Stabilized Step Function Opsin),
			s.v. (single-vessel),
			TH (tyrosine hydroxylase),
			Thy1 (thymus cell antigen 1),
			Th. (thalamus),
			$\tau_{p}$. (laser pulse width),
			ur. (urethane).
		}
	\vspace{.8em}
	\begin{tabular}{lllllllr}\toprule
		Author, Year		& Optogenetic Construct							& fMRI method		& Promoters 		& Brain Region	& Paradigm ($\tau_{p}$,$f_{rep}$) & Anesthesia & Species \\ \midrule
		Lee et al. 2010\cite{Lee2010}		& ChR2(H134R) 								& BOLD 			& CaMKII$\alpha$ 	& Th. 		& block (\SI{15}{\milli\second}, \SI{20}{\hertz}) & iso.-$N_2O$ & rat \\
					& ChR2(H134R) 								& BOLD 			& CaMKII$\alpha$ 	& M1		& block (\SI{15}{\milli\second}, \SI{20}{\hertz}) & iso.-$N_2O$ & rat \\
					& ChR2(H134R) 								& BOLD 			& CaMKII$\alpha$ 	& (M1)Th. 	& block (\SI{15}{\milli\second}, \SI{20}{\hertz}) & iso.-$N_2O$ & rat \\
					& ChR2(H134R) 								& BOLD 			& (PV-Cre)CaMKII$\alpha$& M1 		& block (\SI{15}{\milli\second}, \SI{20}{\hertz}) & iso.-$N_2O$ & rat \\
		Desai et al. 2011\cite{Desai2011}	& ChR2 									& BOLD 			& CaMKII$\alpha$	& S1 		& block (\SI{8}{\milli\second}, \SI{40}{\hertz}) & iso., awake & rat \\
					& ChR2 									& BOLD 			& Thy1			& S1 		& block (\SI{8}{\milli\second}, \SI{40}{\hertz}) & iso., awake & rat \\
		Gerits et al. 2012\cite{Gerits2012}	& ChR2 									& CBV 			& CaMKII$\alpha$ 	& F5 		& block (\SI{8}{\milli\second}, \SI{40}{\hertz}) & awake & rhesus m. \\
					& ChR2 									& CBV 			& CaMKII$\alpha$ 	& FEF 		& block (\SI{8}{\milli\second}, \SI{40}{\hertz}) & awake & rhesus m. \\
		Kahn et al. 2013\cite{Kahn2013}	& ChR2 									& BOLD 			& Thy-1 		& S1		& block (\SI{2.7}{\milli\second}, \SIrange{8}{80}{\hertz}) & iso. & mouse \\
		Weitz et al. 2015\cite{Weitz2015}	& ChR2(H134R)								& BOLD			& CaMKIIa		& DH		& block (\SIrange{5}{50}{\milli\second}, \SIrange{6}{60}{\hertz}) & iso.-$N_2O$ & rat \\
					& ChR2(H134R)								& BOLD			& CaMKIIa		& IH		& block (\SIrange{5}{50}{\milli\second}, \SIrange{6}{60}{\hertz}) & iso.-$N_2O$ & rat \\
		Iordanova et al. 2015\cite{Iordanova2015}	& ChR2(H134R)								& BOLD			& CaMKII$\alpha$ 	& S1 		& block (\SIrange{5}{50}{\milli\second}, \SIrange{3}{20}{\hertz}) & iso. & rat \\
		Liang et al. 2015\cite{Liang2015}	& ChR2(H134R) 								& BOLD 			& CaMKII$\alpha$	& IL mPFC 	& block, ER (\SIrange{10}{20}{\milli\second}, \SI{10}{\hertz}) & iso., awake & rat \\
		Duffy et al. 2015\cite{Duffy2015}	& ChR2(H134R) 								& BOLD 			& CaMKII$\alpha$ 	& IH 		& block (\SI{15}{\milli\second}, \SI{20}{\hertz}) & med. & rat \\
		Ferenczi et al. 2016\cite{Ferenczi2015} 	& ChR2(H134R)								& BOLD 			& (Th-Cre)EF1$\alpha$	& VTA 		& block (\SI{10}{\milli\second}, \SI{20}{\hertz}) & awake & rat \\
					& eNpHR3.0\footnote{inhibitory \label{fn:inh}}				& BOLD 			& (Th-Cre)EF1$\alpha$	& VTA 		& block (\SI{10}{\milli\second}, \SI{20}{\hertz}) & awake & rat \\
					& C1V1$_{TT}$								& BOLD 			& (Th-Cre)EF1$\alpha$	& VTA 		& block (\SI{10}{\milli\second}, \SI{20}{\hertz}) & awake & rat \\
					& SSFO\textsuperscript{\ref{fn:soe}} 					& BOLD 			& (Th-Cre)EF1$\alpha$  	& PFC 		& block & awake & rat \\
		Lohani et al. 2017\cite{Lohani2016}	& ChR2 									& BOLD, CBV 		& (Th-Cre)EF1$\alpha$ 	& VTA 		& block (\SI{5}{\milli\second}, \SI{20}{\hertz}) & iso. & rat \\
		Aksenov et al. 2016\cite{Aksenov2016}	& hChR2(H134R) 								& BOLD			& CaMKII$\alpha$	& barrel S1 	& block (unreported, \SI{50}{\hertz}) & awake & rabbit \\
		Albaugh et al. 2016\cite{Albaugh2016}	& hChR2(H134R) 								& CBV 			& CaMKII$\alpha$	& NAcc 		& block (\SI{10}{\milli\second}, \SI{40}{\hertz}) & iso.-med. & rat \\
		Christie et al. 2017\cite{Christie2017}	& hChR2(H134R) 								& BOLD 			& CamKII$\alpha$	& hp. S1 	& ER (\SI{10}{\milli\second}, N/A) & $\alpha$-chloralose & rat \\
		Takata et al. 2018\cite{Takata2018}	& ChR2(C128S)\footnote{step-function opsin, excitatory\label{fn:soe}}	& BOLD 			& Chrm4 		& V1 neurons 	& block & awake & mouse \\
					& ChR2(C128S)\textsuperscript{\ref{fn:soe}} 				& BOLD 			& Mlc1 			& V1 astrocytes & block & awake & mouse \\
		Choe et al. 2018\cite{Choe2018}	& Archaerhodopsin\textsuperscript{\ref{fn:inh}} 			& BOLD			& (L7-Cre)CAG 		& fp. cerebellum& block (\SIrange{5}{20}{\hertz})  & med. & mouse \\
		Brocka et al. 2018\cite{Brocka2018}	& C1V1(E162T) 								& BOLD 			& CamKII$\alpha$	& VTA 		& block (\SI{10}{\milli\second}, \SI{25}{\hertz})&  med. & rat \\
					& hChR2(H134R) 								& BOLD 			& (Th-Cre)EF1$\alpha$ 	& VTA 		& block (\SI{10}{\milli\second}, \SI{25}{\hertz})&  med. & rat \\
		Grandjean et al. 2019\cite{Grandjean2019}	& hChR2(H134R) 								& BOLD, CBV 		& (ePet-Cre)EF1$\alpha$ & DR 		& block (\SI{5}{\milli\second}, \SI{20}{\hertz}) & iso.-med. & mouse \\
		Leong et al. 2019\cite{Leong2019} 	& ChR2(H134R) 								& BOLD 			& CaMKII$\alpha$ 	& MVN 		& block (\SI{10}{\milli\second}, \SI{20}{\hertz}) & iso. & rat \\
		Y. Chen et al. 2019\cite{Chen2019}	& ChR2 									& CBV 			& CAG			& Th.		& block (\SIrange{5}{20}{\milli\second}, \SIrange{3}{10}{\hertz}) & $\alpha$-chloralose & rat \\
		Albers et al. 2019\cite{Albers2019}	& C1V1(E122T/E112T) 							& BOLD, dfMRI 		& CamKII$\alpha$	& fp. S1 	& block (\SI{10}{\milli\second}, \SI{9}{\hertz})  & med. & rat \\
		X. Chen et al. 2019\cite{Chen2019a}	& hChR2(H134R)								& BOLD, CBV (s.v.)	& CAG 			& CA1 		& block, ER (\SIrange{1}{20}{\milli\second}, \SIrange{1}{50}{\hertz}) & $\alpha$-chloralose & rat \\
					& C1V1$_{TT}$ 								& BOLD, CBV (s.v.)	& CaMKII$\alpha$	& CA1		& block, ER (\SIrange{1}{20}{\milli\second}, \SIrange{1}{50}{\hertz}) & $\alpha$-chloralose & rat \\
		Y. Chen et al. 2020\cite{Chen2020}     & hChR2(H134R)                                                          & BOLD          	& CaMKII                & (barrel S1)CC & block (\SIrange{1}{40}{\hertz}, \SIrange{1}{20}{\milli\second}) & $\alpha$-chloralose & rat \\
		Ioanas et al. 2021\cite{opfvta}	& hChR2(H134R) 								& CBV 			& (DAT-Cre)EF1$\alpha$ 	& VTA 		& ER, block (\SI{5}{\milli\second} \SIrange{15}{25}{\hertz}) & iso.-med. & mouse\\
		Ioanas et al. 2021\cite{drlfom}	& hChR2(H134R) 								& CBV 			& (ePet-Cre)EF1$\alpha$ & DR 		& block (\SI{5}{\milli\second}, \SI{20}{\hertz}) & iso.-med. & mouse \\
		Cover et al., 2021\cite{Cover2021}	& ChR2(H143R)								& BOLD			& hSyn			& IL mPFC	& ER (\SI{5}{\milli\second}, \SI{25}{\hertz}) & awake & mouse \\

		%https://www.pnas.org/content/116/20/10122 Optogenetic fMRI interrogation of brain-wide central vestibular pathways \\
		%\cite{Leong2019}
		%https://pubmed.ncbi.nlm.nih.gov/31182714/ MRI-guided robotic arm drives optogenetic fMRI with concurrent Ca 2+ recording \\
		%\cite{Chen2019}
		%Prefrontal cortical regulation of brainwide circuit dynamics and reward-related behavior DOI:10.1126/science.aac9698 \\
		%\cite{Ferenczi2015}
		%Neural and hemodynamic responses to optogenetic and sensory stimulation in the rat somatosensory cortex
		%\cite{Iordanova2015}
		%aOptogenetic fMRI and electrophysiological identification of region-specific connectivity between the cerebellar cortex and forebrain DOI: 10.1016/j.neuroimage.2018.02.047 \\
		%\cite{Choe2018}
		%\cite{Aksenov2016}i
		%Mapping the Brain-Wide Network Effects by Optogenetic Activation of the Corpus Callosum https://dx.doi.org/10.1093%2Fcercor%2Fbhaa164 \\
		%\cite{Chen2020}
		\bottomrule
	\end{tabular}
\end{sidewaystable}

\begin{figure} [ht]
	\begin{center}
		\begin{tabular}{c} %% tabular useful for creating an array of images
			\includegraphics[height=5cm]{img/ofmri}
		\end{tabular}
	\end{center}
	\caption[example]{
		\label{fig:ofmri}
		\textbf{Graphical representation of a single-session opto-fMRI workflow, highlighting its sequential integration of optogenetics and fMRI.}
		Panels show the usage of a transgenic strain expressing Cre recombinase (left), viral vector delivery of an optogenetic construct using the Cre/LoxP system and optical cannula implantation targeting the entire transfected system (middle), and fMRI measurement with concurrent light stimulation (right).
		Green represents cells with Cre expression (green arrows indicate structural projections), dark grey dots represent optogenetic construct expression, cyan represents light stimulation and light-evoked postsynaptic activity at the stimulation site, pink represents MR signal.
		Figure adapted from \cite{drlfom}, with permission.
	}
\end{figure}

\begin{figure} [ht]
	\begin{center}
		\begin{tabular}{c}
			\includegraphics[height=5cm]{img/NVU.png}
		\end{tabular}
	\end{center}
	\caption[example]{
		\label{fig:nvu}
		\textbf{Schematic of cellular interactions mediating neurovascular coupling.}
		Excitatory input triggers synaptic release of glutamate (Glu), which activates neuronal NMDA receptors (NMDA-R) as well as astrocytic ion channels  and metabotropic Glu receptors (e.g., mGluR5), prompting the release of vasodilator substances such as nitric oxide (NO), epoxyeicosatrienoic acids (EETs), prostaglandins (PGs).
		The vasoactive compounds interact with capillary pericytes (and arteriole and pial artery smooth muscle cells).
		Local capillary dilation may also result from direct interaction with endothelial cells (EC) and then be back-propagated to feeding arteries/arterioles via hyperpolarization and mediators such as NO.
	}
\end{figure}

\begin{figure} [ht]
	\begin{center}
		\begin{tabular}{c}
			\includegraphics[height=5cm]{img/astrosignal.png}
		\end{tabular}
	\end{center}
	\caption[example]{
		\label{fig:astro}
		\textbf{Neuronal and astrocytic signals differ in time course, adding up to the cumulative BOLD response.}
		Forepaw stimulation a mouse under ketamine/xylazine anesthesia, stimulated in \SI{8}{\second} blocks (gray shaded area), with an internal frequency of \SI{3}{\hertz}, an amplitude of \SI{0.7}{\milli\ampere}, and a pulse duration of \SI{0.5}{\milli\second}.
		(a) Normalized Ca\textsuperscript{2+} transients of neuron (blue) and astrocyte (green) population.
		Note post-stimulus undershoot in astrocytic Ca\textsuperscript{2+} response.
		(b) BOLD response with black dots indicating experimental data points and thick black solid fitted curve comprising weighted contributions from neurons (blue) and astrocytes (green) contribution as derived from Ca\textsuperscript{2+} recordings convolved with the respective hemodynamic response functions (HRF).
		The HRFs were assumed as cell-type specific gamma-variate functions.
		Adapted from Skachokova et al. 2021 (pending publication), with permission.
	}
\end{figure}

\begin{figure} [ht]
	\begin{center}
		\begin{tabular}{c}
			\includegraphics[width=\textwidth]{img/balloonmodel.png}
		\end{tabular}
	\end{center}
	\caption[example]{
		\label{fig:balloon}
		\textbf{The canonical balloon model can be augmented to comprehensively account for signal modulation sources from the NVU.}
		Depicted is an extended balloon model integration non-neuronal contributions to the BOLD signal with the originally proposed model (gray shading).
		The stimulus pulse train prompts both neuronal and astrocytic activation (as illustrated by the Ca\textsuperscript{2+} transient) which lead to a respective change in CBF (f\textsubscript{n} , f\textsubscript{a} ) and changes in oxygen consumption (q\textsubscript{n} , q\textsubscript{a} ).
		These effects are lumped into cell-type specific hemodynamic response functions (HRF\textsubscript{n}, HRF\textsubscript{a}, see \cref{fig:astro}).
		In addition, stimulus-evoked changes in cardiac output (heart rate and/or blood pressure) may overrule cerebral autoregulation, prompting a non-specific CBF response (f\textsubscript{p}), which adds to the overall BOLD response.
	}
\end{figure}


\begin{figure}
	\begin{center}
		\includegraphics[width=.98\textwidth]{img/ofmri_experiments_merged}
	\end{center}
	\caption[example]{
		\textbf{Opto-fMRI can be leveraged to image multimodal activity patterns elicited by widely projecting neuronal systems with low endogenous activity profiles}.
		Optogenetics permits both neurotransmitter-specific selectivity, which can be used to target specific neuronal subpopulations, as well as high-amplitude signal enhancement, which can drive network population to sufficiently high levels of activity, as to be clearly modeled in time-resolved fMRI data.
		Depicted are both population-level activity maps (a,b), showing uniform and divergent valence of responses, respectively, as well as a subject-level signal trace example (c).
		\textbf{(a)}
			Population-level t-statistical map of right Ventral Tegmental Area dopaminergic neuronal stimulation.
			Figure adapted from \cite{opfvta}, with permission.
		\textbf{(b)}
			Population-level t-statistical map of Dorsal Raphe Nucleus serotonergic neuronal stimulation.
			Figure adapted from \cite{drlfom}, with permission.
		\textbf{(c)}
			Single-subject time course of mean signal from the Dorsal Raphe Nucleus region of interest, during optogenetic stimulation of serotonergic neurons.
			The CBV signal trace is shown in blue, the response regressor (used to estimate the amplitudes mapped in b) trace is shown in orange, and the response amplitude decay trace is shown in green.
			Figure adapted from reference analysis results of the SAMRI package \cite{samri,irsabi}.
		}
	\label{fig:ofmri_experiments}
\end{figure}


%\begin{figure}
%	\begin{centering}
%	\begin{subfigure}[b]{0.48\textwidth}
%		\centering
%		\includegraphics[width=\textwidth]{img/vta.png}
%		\caption{
%			}
%		\label{fig:vta}
%		\vspace{.4em}
%	\end{subfigure}
%	\hspace{.6em}
%	\begin{subfigure}[b]{0.48\textwidth}
%		\centering
%		\includegraphics[width=\textwidth]{img/dr.png}
%		\caption{
%			}
%		\label{fig:dr}
%		\vspace{.4em}
%	\end{subfigure}
%	\end{centering}
%	\begin{subfigure}[b]{\textwidth}
%		\vspace{1.2em}
%		\centering
%		\includegraphics[width=\textwidth]{img/dr_ts}
%		\caption{
%			}
%		\label{fig:dr_ts}
%		\vspace{-.2em}
%	\end{subfigure}
%		\vspace{.4em}
%		\caption{
%			\textbf{Opto-fMRI can be leveraged to image multimodal activity patterns elicited by widely projecting neuronal systems with low endogenous activity profiles}.
%			Optogenetics permits both neurotransmitter-specific selectivity, which can be used to target specific neuronal subpopulations, as well as high-amplitude signal enhancement, which can drive network population to sufficiently high levels of activity, as to be clearly modeled in time-resolved fMRI data.
%			Depicted are both population-level activity maps (\cref{fig:vta,fig:dr}), showing uniform and divergent valence of responses, respectively, as well as a subject-level signal trace example (\cref{fig:dr_ts}).
%			\textbf{(a)}
%				Population-level t-statistical map of right Ventral Tegmental Area dopaminergic neuronal stimulation.
%				Figure adapted from \cite{opfvta}, with permission.
%			\textbf{(b)}
%				Population-level t-statistical map of Dorsal Raphe Nucleus serotonergic neuronal stimulation.
%				Figure adapted from \cite{drlfom}, with permission.
%			\textbf{(c)}
%				Single-subject time course of mean signal from the Dorsal Raphe Nucleus region of interest, during optogenetic stimulation of serotonergic neurons.
%				The CBV signal trace is shown in blue, the response regressor (used to estimate the amplitudes mapped in \cref{fig:dr}) trace is shown in orange, and the response amplitude decay trace is shown in green.
%				Figure adapted from reference analysis results of the SAMRI package \cite{samri,irsabi}.
%			}
%		\label{fig:three graphs}
%\end{figure}


\begin{figure} [ht]
	\centering
	\includegraphics[width=\textwidth]{img/ma_voxels}
	\vspace{.2em}
	\caption{
		\textbf{Opto-fMRI provides macroscopic resolution disambiguation of cell biological processes}
		Depicted are neuronal schematics showing a somatic compartment and a synaptic compartment, as these may be seen in fMRI (distance between voxels not to scale).
		Depending on the statistical contrast of the stimulation as well as on the the neuronal system targeted, such different voxels may be more distant than the spatial autocorrelation range of fMRI --- thus capturing potentially different responses in different cellular compartments.
		Such a difference may be seen in \cref{fig:ofmri_experiments}b, where the red coded somatic voxel would correspond to the read heatmap voxels in the midbrain, and the blue coded voxel would correspond to the blue heatmap voxels in the cortex.
		The neuronal schematic showcases cell biological processes, such as neurotransmitter synthesis, anterograde synaptic transmission, autoinhibition, neurotransmitter reuptake, and degradation, laid out over cell compartments.
		Neurotransmitters and precursors are color-coded green and proteins involved in the aforementioned processes are coded gray.
		Figure adapted from \cite{drlfom}, with permission.
		\label{fig:cb}
		}
\end{figure}
