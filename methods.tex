{\color{ForestGreen}
\section{Methods Overview}
}
\label{sec:em}

{\color{ForestGreen}
\subsection{Multimodal Measurement}
\label{sec:mm}
}
{\color{violet}

The BOLD fMRI signal is determined by both neuro-vascular and neuro-metabolic adaptation, whereas pure hemodynamic readouts such as activity-induced CBF and CBV changes reflect only volume integrated FH responses.
Combining the different fMRI readouts with alternative (invasive) measures of stimulus-evoked activity within the neurovascular unit provides important insight regarding the interpretation of fMRI signals.
Readouts tightly linked to neuronal activity include optical imaging using voltage-sensitive dyes, measurements of calcium (Ca\textsuperscript{2+}) transients as prompted by membrane depolarization, and assessment of neurotransmitter turnover (glutamate/glutamine).
Oxygen and glucose consumption assessment, by contrast, is not neuron-specific, but remains nevertheless of interest with regard to neurometabolic coupling.

Electrophysiological recordings, which provide information on membrane potentials directly and exclusively, have frequently been combined with fMRI.
For example, Logothetis et al. \cite{Logothetis2001} demonstrated a high degree of correlation between the BOLD signal and local field potentials (LFPs) as a measure of overall synaptic input and local processing.
While such recordings yield direct insight into the electrical activity of neurons and provide high temporal resolution, they are technologically challenging and do not provide cellular specificity, i.e. other constituents of the neurovascular unit cannot be distinguished.
An attractive alternative is the measurement of Ca\textsuperscript{2+}-transients using optical recordings/imaging in combination with calcium-sensitive dyes \cite{Scanziani2009} or GECIs \cite{Tian2012,Adelsberger2014,Yang2017}, which allow the assessment of cell-specific contributions to neural processing.

{\color{ForestGreen}
\subsubsection*{Experimental Design --- Hybrid Rationale}
}

The optimal optical method to be combined with the fMRI setup depends on the scientific question to be tackled.
Hence, before designing a hybrid setup, the following questions should be addressed:

\textit{
{\color{ForestGreen}
The first important question in hybrid setup design is: Does the scientific question require simultaneous data acquisition?
}
}
Sequential acquisition with two (or more) modalities may be advantageous if the objects of study are, e.g., evoked brain responses that are highly stereotypic and constant over time.
Given a stimulus paradigm that excludes refractory or adaptation effects, one might consider trial-by-trial variability of neural responses to identical stimuli as random noise, meaning that it does not contain relevant biological information.
Contributions of random noise can be accounted for by averaging over multiple trials.
Under these circumstances, sequential acquisition would be advantageous in order to avoid trade-offs, which, as discussed below, inherently arise in either modality when using a hybrid measurement setup.

However, recent experimental \cite{Stringer2019} and computational studies \cite{Engel2019} suggest that trial-by-trial variability in stimulus-evoked recordings comprises relevant information about the behavioral state and ongoing network dynamics.
As such, assuming that activity is largely spontaneous in nature, meaningful correlation of neural and hemodynamic fluctuations would require simultaneous acquisition and thus multimodal readouts of brain activity.
This is of course also the case for task-free experiments, in which resting-state networks are determined by analyzing fully endogenous and thus “spontaneous” slow-wave fluctuations.

Further, the majority of the studies mentioned in this review are conducted in rodents, which typically require anesthesia.
This affects multimodal experiments twofold: Firstly, the anesthetic modulates spontaneous Ca\textsuperscript{2+} activitiy; and secondly, the anesthetic changes the breathing pattern and thereby the $CO_2$ levels in the bloodstream, which drastically impacts BOLD responses \cite{Alst2019}.

\textit{
{\color{ForestGreen}
The second major consideration is: Does the scientific question ask for an optimal method providing cellular resolution?
}
}
There are two strategies to gather cell-specific information using optical methods: microscopy-based methods, which provide sufficient spatial resolution to identify individual cells; and bulk sampling methods, which permit identification of specific cell population using selective labelling techniques.
The fMRI signal of a voxel represents the integrated activity of thousands of neurons.
Hence, when investigating the contribution of a specific cell population to the lumped fMRI signal, it may be more appropriate to acquire the bulk signal of a volume comparable to that giving rise to the fMRI signal rather than monitoring the behavior of individual cells within a much smaller FOV.
Nevertheless, as already discussed, high spatio-temporal resolution is of relevance for analyzing mechanisms underlying NVC and its dynamics \cite{Devor2011,Uhlirova2016,Gagnon2016}.
For example, MRI/fMRI studies using high spatial resolution (\SI{100x100}{\micro\meter\squared} and \SI{50x50}{\micro\meter\squared}, respectively) in anesthetized rats enabled correlating fluctuations of vessel-specific fMRI signals with the intracellular calcium signal measured in neighboring neurons \cite{He2018}.

Signals reflecting brain activity are typically weak, and the signal-to-noise ratio (SNR) is commonly a limiting factor for true hybrid measurements, i.e. simultaneous recording of signals with two (or more) modalities.
Adapting modalities to work in parallel exacts a toll on their performance.
Boosting up spatial resolution would further compromise SNR, as the signal is proportional to the volume of the individual voxel sampled.
This can be accounted for by using high magnetic field strengths and a small MRI receiver coil yielding a high filling factor (B\textsubscript{0}=\SI{14.1}{\tesla} and \SI{6}{\milli\meter} diameter radiofrequency surface coil in the study of \cite{He2018}), which inherently limits the FOV.
For most practical cases, SNR constraints favor using the largest voxel (sample) volume possible, and in case spatial resolution is not required, i.e. the bulk optical signal is sufficient to address the scientific question, fiber photometry provides the highest SNR advantage.
In fMRI, however, the voxel size cannot be increased arbitrarily, as magnetic field variations produced by different tissue types within a larger voxel (partial volume effects) can degrade the SNR.

\textit{
{\color{ForestGreen}
A third major question related to hybrid setup design is: Is data collection from deep-lying brain areas relevant for answering the scientific question at hand?
}
}
Deep brain structures are of relevance when studying signal processing in the brain.
For example, peripheral sensory input is routed via the spinal cord and subcortical nuclei such as the thalamus to the respective somatosensory cortical area.
fMRI, enabling 3D coverage of the whole brain, is ideally suited for such studies.
Yet, can we assume that the relationship between cellular activity measures and the hemodynamic fMRI response, which due to accessibility is typically studied in brain cortex, is independent of the brain region studied?
This is unlikely, as both the cellular composition of brain tissue as well as the local vascular architecture are region-specific and, hence, the weight of their contributions to the fMRI signal will vary across the brain.
In fact, in biophysical modeling of the fMRI signal response, brain region-specific hemodynamic response functions (HRF) have to be considered \cite{Handwerker2004}.
Therefore, we cannot assume that what we have learned for cerebral cortex can be simply extrapolated to other brain regions.

Optical access to deeper brain structures is limited by the nature of light photon interaction with biological tissue (e.g. due to diffusive light propagation and low tissue penetration depth).
There are two approaches to study deep-lying structures with optical techniques:
The use of diffuse optical tomography (including fluorescence tomography), which reconstructs light source distribution in 3D from projections measured at the tissue surface, but which yields spatial information significantly inferior to MRI \cite{Ren_2015}.
Alternatively, invasive procedures have to be used, i.e. superficial brain layers covering the region of interest have to be cleared (e.g. via a cortical window \cite{Pilz2018,Velasco2014} or via the surgical introduction of optical fibers and fiber bundles \cite{Miyamoto2016}).
Individual single- or multimode fibers are probably the least invasive of these approaches and may constitute the method of choice for studying subcortical structures including basal ganglia and brainstem nuclei.

{\color{ForestGreen}
\subsubsection*{Experimental Design --- Hybrid Variants}
}

To provide adequate solutions for multiple of the use case variants delineated above, multiple variations of integrating light modalities with fMRI have been developed, which include the following:

\textit{Fiber photometry} (\cref{fig:fpsetup}) is ideally suited for simultaneous readout with fMRI and has so far been the most popular technique for in-vivo hybrid recordings in rodents (see \cref{tab:recording}).
An optical fiber can guide the laser beam into the MR bore, and the fluorescence signals to a detector outside of the magnetic field, so that the two methods do not interfere with each other.
While a single multimode fiber lacks spatial resolution, the placement of the fiber tip allows regional selectivity for a given brain region.
Further spatial specificity can be gained by selecting the core diameter and numerical aperture of the multimode fiber to adjust the size of the cone where light is emitted and collected, such that it matches the area of interest.
Additionally, an array of fibers can be implanted so as to target multiple brain regions of interest, including subcortical areas \cite{Sych2019}.
Cell type or cell compartment specificity can be attained by expressing GECIs under the appropriate promoters \cite{Rose_2014,Tian_2012}.
The hardware for fiber photometry is largely interchangeable with the optogenetic stimulation tools discussed in the subsequent sections.
Thus, the two methods can readily be combined, allowing multimodal readouts under optogenetic control of defined neuronal circuits \cite{Wang2018,Albers2017}.

\textit{Optical microscopy used in conjunction with fMRI} combines the excellent spatio-temporal resolution of fluorescence microscopy with the full brain coverage of fMRI, and thus provides a useful bridge between the microscopic, mesoscopic, and macroscopic scale.
However, optical imaging systems contain many sensitive electronic components and actuators that cannot operate in an MR environment.
Designing hybrid systems in which the optical equipment satisfies the material and spatial constraints of MRI, thus remains an ongoing challenge.
In particular, canonical RF coil designs may also interfere with the light path.
Nevertheless, a proof-of-concept for an MR-compatible two-photon microscope has been developed, in which the optical components are connected  to the MR bore via a fiber light guide \cite{Cui2017}.
In a recent study, a fiber-optic bundle was used to project the fluorescence signals on a camera outside the MR room, allowing widefield mesoscopic imaging of the entire mouse cortex \cite{Lake2020}.
Another prototype used a miniaturized MR-compatible camera, allowing a one-photon microscope to operate within the MR bore \cite{Wapler2021}.

\textit{Diffuse optical imaging combined with fMRI} presents an alternative to fluorescence-based recordings, as the absorption properties of hemoglobin can be used as a functional readout of blood volume and oxygenation changes.
NIRS, owing to the attractive properties of near-infrared light,  has found widespread use as a non-invasive tool in clinical applications \cite{Kim_2017}.
The high penetration depth, which enables it to pass through even the human skull, and the ability to provide absolute quantification of hemodynamic parameters, have made it an early tool of choice to validate assumptions made with BOLD fMRI \cite{Steinbrink_2006}.
Hybrid NIRS/fMRI systems can also expand the scope of MRI applications without the need for exogenous contrast agents \cite{Hashem_2020}.
Typically, a setup consists of one or more light source/photodetector pairs, which, as in fiber-photometry, can be connected via fiber-optic cables.
Typically, the recordings are not spatially resolved, as diffusive light paths inherently limit the resolution, though camera based systems have been developed \cite{Zhu_2016}.
Ultimately, as another hemodynamic readout, NIRS cannot provide the complementary information needed to separate the various contributions to BOLD  fMRI, which thus limits its utility for NVC research.


{\color{ForestGreen}
\subsection{Optogenetic Stimulation}
\label{sec:mm}
}

Discerning how specific cell types affect brain wide functional responses is essential for elucidating the interconnection of brain networks and their roles in information processing and relay.
As already described, light is ideally suited for this purpose due to its minimal interference with the imaging readout method (see \cref{sec:i}) and the fact that brain tissue is not intrinsically sensitive to light.
Manifold molecular biological techniques, rendering tissue specifically susceptible to light photons after light-sensitive sensor protein introduction via genetic engineering, are therefore fully accessible for fMRI.
The combination of optogenetics and fMRI has thus become a method of choice across neuoscientific applications (see \cref{tab:stim}), general concepts and trends of which we further lay out.

{\color{ForestGreen}
\subsubsection*{Vectors}
}

Available optogenetic signal transducers include ion channels such as channelrhodopsins \cite{Nagel2002} and ion pumps such as halorhodopsins \cite{Zhang2007}.
Both stimulation and inhibition can be elicited via such constructs, the former most commonly via cation-permeable channelrhodopsins (e.g. ChR2\cite{Nagel2005}) and the latter via anion-transporting halorhodopsins (e.g. eNpHR3 \cite{Gradinaru2010}) or anion-permeable channelrhodopsins (e.g. GtACRs \cite{Mahn2018}).
A further methodological refinement opportunity is provided by step-function opsins, which encompass both anion \cite{Berndt2008} and cation \cite{Berndt2014} channels.
These constructs remain in an altered conformational state (minute-range off-kinetic time constants) following light stimulation and can be returned to the “inactive” conformational state via light stimulation at a different wavelength  ---  as opposed to classic channelrhodopsins, which effectively convert into the “active“ state only during light input (millisecond-range off-kinetic time constants \cite{Gunaydin2010}).

Opto-fMRI applications consist of the sequential integration of optogenetic targeting and the delivery of light concomitant with fMRI measurement.
Though specific targeting for both vector delivery and stimulation can vary greatly, a representative use case of prevalent practices is presented graphically in \cref{fig:ofmri}.
In this example, the co-localization of cell bodies of a widely projecting neurotransmitter system in a  brainstem nucleus is leveraged to stimulate a wide efferent spectrum with a coherent signal.

Vector delivery methods for opto-fMRI use either the Cre-LoxP system in conjunction with AAV (adeno-associated virus) vectors, or lentiviral vectors \cite{Gerits2012} --- whereby the former provides the advantage of decoupling the cell-type selection from the optogenetic construct, allowing greater interrogation flexibility without the need for custom vector production.
{\color{ForestGreen}While Cre-LoxP-based targeting commonly employs transgenic lines, the Cre construct can itself be delivered via a viral vector --- though available virus preparation libraries are more constrained, and there are additional restrictions on promoter length, particularly if delivered via AAV.}

A critical aspect for the success of opto-fMRI experiments is the elicited signal amplitude, as the intrinsically low SNR of the method \cite{MI} constitues a major challenge.
High expression levels, sufficient photon flux density at the target site, and a stimulation protocol yielding high contrast  are essential for obtaining reliable signals.

{\color{ForestGreen}
\subsubsection*{Experimental Design --- Setup}
}

The foremost consideration in applying opto-fMRI is selecting the correct biological features for both stimulation and imaging.
While the breadth of neurosicentific applications cannot be sufficiently detailed without a review of all neurobiology, a series of guidelines can be formulated based on the characteristics of the method.
Conceptually, opto-fMRI uses targeted, invasive, and high-temporal-resolution stimulation to drive a set of neurons, combined with a whole-brain, non-invasive, medium-temporal-resolution read-out method.
Thus, an important question is whether targeted stimulation is relevant with respect to whole brain imaging.
As a consequence, the ideal setting for leveraging the full potential of the technology is targeting widely-projecting neurotransmitter systems, in order to investigate their effects on neuronal activity at the whole-brain level.
This is substantiated by extant opto-fMRI literature, in which the stimulation of wide efferent spectrum structures such as monoaminergic systems \cite{Lohani2016,Grandjean2019,opfvta,drlfom} and long-range glutamatergic projections \cite{Liang2015,Gao2017} are prominently paired with whole-brain imaging and analysis.
A selected structure with a wide efferent spectrum may however only evoke local activity, such as has been the case for the nucleus accumbens \cite{Albaugh2016}, which provides a setting for contrasting structural, electrically evoked, and optogenetic excitation kinetics.
Opto-fMRI has, on the other hand, also been used with the express intent of stimulating and measuring localized activity, such as in primarily self-projecting cortical regions or in the hippocampus, which is prone to local seizure effects \cite{Christie2013,Duffy2015,Bregestovski2014}.
Such applications, although not leveraging the full spatial potential of fMRI, provide relevant methodological information, support in technology development \cite{Duffy2015}, and yield relevant information with regard to the nature of the fMRI signal \cite{Kahn2013,Lee2010,Chen2019a}.

While fMRI can be freely combined with any optogenetic technologies, the exigences imposed by MRI scanner access specifically encourage the use of the most established and extensively characterized constructs.
As such, novel constructs of great clinical interest, such as anion-permeable channelrhodopsins and step-function opsins, have respectively seen no or little \cite{Ferenczi2015,Takata2018} opto-fMRI use, and present promising opportunities for future research.

A key constraint to leveraging the full versatility of optogenetics, and thus of opto-fMRI, is the application of the technology in species with low or no availability of transgenic lines.
The highly flexible Cre-LoxP/AAV vector delivery method, which allows the decoupling of signal transducer variants from cell-type selection, is contingent on transgenic lines expressing Cre recombinase under the desired cell-type-specific promoter.
Large libraries for such lines are available for the mouse, and increasingly, but to a lesser extent, for the rat.
Other model animals, and particularly higher primates, do not offer any comparable level of access, and consequently, opto-fMRI application in these settings presupposes the use of more unwieldy custom lentiviral vectors, as well as close consideration of the trade-off between promoter length and expression characteristics \cite{Gerits2012,Gerits2013}.

{\color{ForestGreen}
\subsubsection*{Experimental Design --- Stimulation Protocols}
}

Optogenetics offers considerable flexibility with regard to the stimulation protocol, apt usage of which can considerably enhance the SNR for opto-fMRI.
A fundamental distinction is made in the field of fMRI between “event-related" and “block" designs, which refer to the distribution of stimulation events over the duration of the experiment.
Nominally, a design is deemed event-related if the stimulation (‘ON‘) period consists of a single event or is otherwise equal to or shorter than the acquisition TR --- with longer ON durations being deemed block designs.
Event-related designs lend themselves to sequence randomization, as they provide low but similar contrast upon variation, more closely resembling physiological activation.
While such protocols can be employed in opto-fMRI (and may consist of single events\cite{Christie2017} or short trains\cite{Liang2015}) block designs are generally preferred due to the ability to deliver superior contrast \cite{opfvta,Grandjean2019,fsl,spm}.
In self-stimulation applications, short events can be concentrated into self-stimulation ON periods, yielding designs which are nominally event-related but may residually benefit from block-design contrast characteristics depending on the statistical analysis \cite{Cover2021}.

The precise time-sequence of a block design can further be optimized with respect to both its contrast characteristics and its feasibility for the given biological system being targeted.
The contrast characteristics of the stimulation protocol refer to the theoretical quality of its statistical estimation in the general linear model (GLM), which is the analysis method most commonly used to resolve spatial response patterns based on a stimulation time course.
Experimental parameters which bear on the fitness of a stimulation time course include experiment length, temporal frequency band reliability (lower frequency bands in particular may be considered unreliable in fMRI due to drift), as well as the impulse response function (IRF), which differs greatly with respect to contrasts (BOLD, CBV, or custom functional contrast agents).
These parameters can be submitted to genetic-algorithm optimization workows\cite{Wager2003,Kao2009}, which can produce highly performing stimulation time courses.

The internal structure of stimulus blocks can also be adapted to increase the contrast generated in fMRI, though this may produce varying degrees of physiological comparability in the elicited activity mode.
The intrinsic SNR constraints of fMRI, particularly when performed without cryogenic coils, however, prompt stimulus train optimization towards evoking the maximal activity level compatible with tissue preservation.
Stimulation protocols for opto-fMRI, based predominantly on in-house empirical optimization, generally span \SIrange{10}{100}{\hertz} in frequency and \SIrange{2}{20}{\milli\second} in pulse width \cite{Liu2015,Liang2015,Lohani2016,Albaugh2016,Grandjean2019}.
Such optimizations are formalized in a number of ancillary methodological investigations, which predominantly identify strong effects for stimulation frequency and weak effects for pulse width variations \cite{Liu2015,Grandjean2019,Chen2019,Chen2019a}.
While this is consistent with theoretical considerations arising from extant construct off-kinetics \cite{Gunaydin2010} --- and thus constitutes the best working hypothesis for stimulus structure optimization --- some studies do not reproduce this trend \cite{Chen2020,Iordanova2015}, and further results indicate non-linear and differential neuronal population recruitment based on pulse frequency \cite{Weitz2015}.
Far from simply being a technical parameter for which basic optimization heuristics could reliably be translated into \textit{a priori} optimized protocols across brain areas, stimulus structure may also be leveraged as a flexible tool for the exploration of differential signal propagation.

A ceiling for the maximization of optogenetically evoked signal can be estimated with regard to heating artefacts, which can arise following light stimulation \cite{Rungta2017}.
Heating artefacts are of particular concern for ion-pump optogenetic constructs, as these require considerably stronger light stimulation \cite{Han2011}.
Systematic study of the phenomenon initially reported its emergence at approximately \SI{20}{\milli\watt\per\square\milli\meter} average light deposition per second (\SI{445}{\nano\meter} and \SI{532}{\nano\meter} light stimulation) \cite{Christie2013}, and subsequently at as little as \SI{9}{\milli\watt\per\square\milli\meter} (\SI{445}{\nano\meter} light stimulation) \cite{Christie2017}.
Later still, initial trends were corroborated by a systematic study estimating the emergence of heating artefacts bewteen \SI{19.5}{\milli\watt\per\square\milli\meter} and \SI{25}{\milli\watt\per\square\milli\meter} (\SI{552}{\nano\meter} light stimulation) \cite{Albers2019}.
These estimates may, however, vary at different wavelengths, across brain areas, and be contingent on within-pulse laser power\footnote{Cf. \cite{Christie2017} and \cite{Albers2019}} and stimulation block duration (in addition to the time-averaged light deposition) — and may further depend on the optic cannula diameter in a non-linear fashion \cite{Ash2017}.
Heating artefacts (alongside unspecific visual-activation based signal) thus remain a significant methodological risk in opto-fMRI.
In addition to preliminary empirical verification in a control group, these confounds can be mitigated in General Linear Model (GLM) analysis given sufficient control group size \cite{fsl,spm,opfvta}.

An important consideration throughout stimulation protocols is that optogenetics can \textit{drive}, but cannot \textit{clamp} activity.
This means that while both excitation and inhibition can be elicited, this does not happen in a complementary manner.
In the absence of activation-inducing stimuli, cells are not inactivated, but rebound towards endogenous network activity.
Similarly, in the absence of inactivation-inducing stimuli, cells will also rebound towards normal endogenous network activity.
This constraint even applies to step-function opsins \cite{Berndt2014}, which work as ion channels, and whose primary advantage is requiring minimal light to switch into an “open channel” state.
Step-function optogenetic techonlogy is theoretically feasible for providing \textit{clamping} functionality with minimal light deposition in a fused-protein assay, though such tools are not currently available, and functionality approaching clamping is only available via construct co-delivery \cite{Berglund2019}.


}

\subsection{{\color{ForestGreen}Implant Design}}
\label{sec:ma}

A common though easily overlooked constraint in small animal applications, which affects both multimodal recording and optogenetic stimulation experiments, is the potential for setup incompatibilities between optical tools and the cutting edge of fMRI technologies.
This manifests itself in implant/coil incompatibility, whereby surface coils --- cryogenic coils in particular --- constrain the positioning of animals with optical implants as are canonically used for light-based measurement or stimulation.
Novel manufacturing as well as operation protocol developments permit pitch and yaw variable targeting of structures \cite{stereotaxyz}, though large-scale hybrid measurement or opto-fMRI studies leveraging cryogenic surface coil capabilities have not yet been published.
An additional unexpected pitfall is the magnetic susceptibility of dental cement, which is commonly used to stabilize optic cannulae or skull windows, particularly with regard to longitudinal applications.
For clinical practice, dental cements are commonly adulterated with metal oxides and silicates in order to produce radioopaque characteristics \cite{MontesFariza2016}, which can also lead to artefacts in MRI \cite{Tymofiyeva2013}.
This issue can generally be avoided by using dental cements not explicitly labeled as radioopaque, but as radioopacity is a desired diagnostic trait, pre-implantation cement testing is recommended.
Further, during any implantation procedure, additional care must be taken to not enclose any air pockets or blood clots, as those might similarly introduce susceptibility artifacts in MRI.


