\section{Multimodal readouts of brain activity - insights into neurovascular coupling}
\label{sec:nvc}

\subsection*{Components of neurovascular coupling}
\label{sec:nvc1}

The basic neurovascular unit (NVU) consists of neuron, astrocyte and vascular cells (such as endothelial cells, pericytes, and smooth muscle cells), and is schematically summarized in \cref{fig:nvu}.
The concept of CBF regulation in response to an increased energy demand by active neurons has been revisited in view of the fact that neurons can maintain their initial activity without requiring additional blood supply and, as a consequence, CBF responses are slow.
Neurotransmitter-mediated signaling plays a major role in regulating CBF, the main excitatory neurotransmitter being glutamate.
It has been suggested that increased extracellular levels of glutamate following its release at synapses switch astrocytes to anaerobic glycolysis.
The relative amount of O$_2$ available for neurons thereby increases, while astrocytic lactate would serve as an energy substrate for neurons (a process known as the “astrocyte-neuron lactate shuttle”) \cite{Pellerin1994}.
Demonstration of a lactate gradient from astrocytes to neurons supports this hypothesis \cite{Mchler2016}.
CBF adaptation would then serve to replenish the (astrocytic) energy reservoirs, but also to maintain function during prolonged stimulaion \cite{Schulz2012,Mishra_2016}.

\subsubsection*{Astrocytes as mediators of functional hyperemia}
\label{sec:nvc1a}

The involvement of astrocytes in NVC has long eluded electrophysiological recordings, as they are electrically inexcitable.
However, given their key location within the NVU, engulfing the synapses and covering the vasculature with their end-feet, it appears plausible that astrocytes are involved in FH to an extent beyond the mere role of an energy reservoir.
Glutamate mediated signaling leads to activation of neuronal N-methyl-D-aspartate receptors (NMDA-R) as well as astrocytic metabotropic glutamate receptors (mGluR) and ion channels \cite{Attwell2010}.
Down-stream processes involve the activation of neuronal nitric oxide synthase (nNOS) and astrocytic phospholipase A2 (PLA2), leading to the formation of nitric oxide (NO) and arachidonic acid derivatives (prostaglandins, PGs, and epoxyeicosatrienoic acids, EETs) --- compounds which are known as vasodilators \cite{Attwell2010}.
These early findings provided a strong indication of astrocyte involvement in FH.

Investigating the intricate role of astrocytes in the generation of BOLD signals is a prime example of a research question that strongly benefits from hybrid optical/MRI approaches.
Their involvement could be demonstrated by combining neuronal and astrocytic Ca\textsuperscript{2+} recordings with BOLD fMRI \cite{Schulz2012}.
Previous studies have shown a sluggish astrocytic Ca\textsuperscript{2+} response to somatosensory stimuli, with a much longer time-to-peak and decay time relative to the neuronal response (\cref{fig:astro}a).
Such fiber-optic bulk recordings represent the averaged activity of all astrocytes in the area around the fiber tip.
It should be noted that individual astrocytes can vary in their temporal characteristics, which can be observed using two-photon microscopy \cite{Stobart2016}.
Yet, within the spatial scale of BOLD fMRI voxels, it becomes apparent that the kinetics of the summed astrocytic Ca\textsuperscript{2+} response correspond to the later phases of the BOLD response (\cref{fig:astro}b) --- i.e. the prolonged signal elevation after stimulus cessation, and the slow return to baseline.
These so-called nonlinear components of the BOLD response have long remained enigmatic as they do not linearly scale with the neuronal response, unlike the BOLD amplitude and time to peak.
A growing body of NVC research aims to characterize the diverse roles performed by astrocytes.
Recordings of intrinsic astrocytic Ca\textsuperscript{2+} activity concurrent with BOLD fMRI, \cite{Wang2018} have found spontaneous Ca\textsuperscript{2+} transients to be coupled with cortex-wide negative BOLD signals and a positive BOLD signal in the thalamus.
Subsequent elctrophysiology confirmed that thalamic activity preceded the Ca\textsuperscript{2+} transients, which hints at astrocytes acting as mediators of thalamic regulation of cortical states.
Further, optogenetic stimulation of astrocytes has found to be sufficient to evoke positive BOLD responses, without significant activation of nearby neurons \cite{Takata2018}. While the physiological role of such independent astrocytic activation is still unclear, it may provide an explanation for certain task-related hemodynamic responses that occur without neuronal activity \cite{Sirotin2009}.


\subsubsection*{Vasodilatory signals backpropagate along the vasculature}
\label{sec:nvc1b}

Electrical forepaw stimulation was shown to prompt a local dilation of the capillary bed and an increase in the velocity of red blood cells indicative of decreased vascular resistance \cite{Stefanovic2007}.
Whether this dilation is passive or active is currently unclear, though the effect appears to be controlled by pericytes \cite{Peppiatt2006,Hamilton2010}.
Recent data support the important role of pericytes in NVC, demonstrating that astrocytes signal to pericytes rather than arterioles \cite{Attwell2010}, and that pericyte degeneration leads to neurovascular uncoupling \cite{Kisler_2017}.

While neuronal and astrocytic vasodilatory signaling is focused to the site of activity, spatiotemporal analysis of vascular responses revealed involvement of vessels at distances larger than 1mm \cite{Hillman2014,Li2003} via retrograde propagation of vasodilation \cite{Chen2014}.
The endothelial cell (EC) layer constitutes the obvious guidance structure for signal backpropagation to feeding arterioles and pial arteries, as disruption of EC signaling led to significantly reduced FH.
Two vasodilatory mechanisms have been suggested: a fast process mediated by endothelial hyperpolarization and a slow process associated with the release of vasodilator compounds \cite{Hillman2014}.

Taken together, FH arises as combination of different mechanisms affecting vascular segments in a differential manner \cite{Attwell2010,Hillman2014,Mishra_2016}.
Neurotransmitter (glutamate) release triggers a sequence of events that lead to an orchestrated response of neurons, astrocytes, and vascular cells (ECs, pericytes, smooth muscle cells) causing local FH.
The vasodilatory stimulus is then backpropagated to arterioles and pial arteries via EC signaling.
As the vasculature is dilating along the entire path of this backpropagation, the resulting spatial blurring can make fMRI responses appear more widespread than the underlying neural activitiy \cite{OHerron2016}. In addition, the vascular organization differs greatly across brain regions, which may be a key contributor to the regional variability of the HRF observed in fMRI studies \cite{Handwerker2004}.
To date, studies involving vascular signal propagation have been exclusively conducted with high-resolution microscopy, focusing on individual blood vessels. With the continual improvements of GECIs and gene targeting, it has become possible to apply the multimodal techniques discussed in this review to other cell types, such as various vascular cells \cite{Wier2017}.
Given the integrative nature of the fMRI signal, multimodal recording of activity-induced cellular responses becomes mandatory for the deconvolution of individual contributions governing the fMRI signal response.

\subsubsection*{Multimodal imaging for the refinement of forward modelling}
\label{sec:fwm}

The aim of noninvasive functional brain imaging is to provide an accurate estimate of the underlying neuronal activity.
Employing optical recordings at multiple stages of neurovascular coupling provides relevant information for optimizing forward modeling.
To generate a more comprehensive picture of the observed BOLD response, we suggest extending the well-known balloon model, which predicts the changing venous blood volume and dHb content (thereby prompting the BOLD response), by including non-neuronal mechanisms shown to display stimulus-associated signal transients (\cref{fig:balloon}).
The original model (see e.g. \cite{Stephan2007}) depends only on the inflow of blood, with neuronal signaling (i.e. neurotransmitters acting directly on the vasculature) typically assumed as the only driving force.
However, this fails to account for certain key features of the BOLD response, such as the prolonged response phase or post-stimulus undershoot.
Based on our previous research, we suggest two additional main sources associated with CBF changes: Astrocytic activity \cite{Schulz2012,Schlegel2018} leading to the release of vasoactive molecules, and depending on the physiological state potentially to altered cardiovascular activity causing a systemic change in blood flow that may overrule cerebral autoregulation.
The last point is of particular relevance for studies using anesthetized animals.
While the former two sources are part of intact neurovascular coupling, cardiovascular responses, such as stimulus-evoked sudden changes in heart rate and blood pressure \cite{Schroeter2014}, constitute a confounding factor that should be minimized \cite{Schlegel2015,Shim2018}.

Animals studies revealed interference of anesthesia with CNS physiology at various levels.
It was shown to affect neuronal excitability per se without affecting NVC \cite{Franceschini2010}, to compromise NVC \cite{Masamoto2012}, or to alter systemic cardiovascular output \cite{Schroeter2014}, or to act by a combination of these effects.
In all these conditions, anesthesia would induce alterations in the stimulation induced FH, though the causes would be rather different, and correspondingly the relationship between neuronal signals and FH responses.
Such limitations have to be considered when using results obtained from anesthetized animals for the interpretation of data obtained in conscious humans.
Yet, a detailed discussion of these aspects is beyond the scope of this article.

A forward model of the neuronal-astrocytic-vascular Ca\textsuperscript{2+} signaling cascade has recently shown to accurately reproduce BOLD responses \cite{Tesler2021}.
However, the assumption of astrocytes as a passive intermediary between neurons and the vasculature fails to account for situations where the neuronal activity (but not necessarily the astrocytic activity) and the BOLD response are uncoupled or even inverted \cite{Takata2018,Sirotin2009}.
The hope of creating more accurate forward models is that, by inverting the model, accurate inferences about neuronal activity could be made solely based on the BOLD response.
Despite the growing evidence that the BOLD signal is not driven by neuronal activity alone, refined forward models could help define the boundaries within which the NVC is intact, and thus prevent erroneous interpretations of BOLD fMRI.
