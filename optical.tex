{\color{ForestGreen}
\section{Optical Signals for Neuronal Stimulation --- Insights into monoaminergic systems}
}
\label{sec:o}

In preclinical neuropsychiatric applications, opto-fMRI has prominently been used to map the effective connectivity of neurotransmitter systems, as illustrated by the landmark results of mapping the dopaminergic \cite{Lohani2016} and serotonergic \cite{Grandjean2019} systems (\cref{fig:ofmri_experiments}a,b).
On account of better statistical contrast, such mapping efforts produce more finely resolved spatial maps than corresponding chemogenetics approaches (cf. \cite{Grandjean2019} and \cite{Giorgi2017}), albeit at the cost of increased invasiveness.

The reuse of these assays has demonstrated map reproducibility \cite{Grandjean2019,drlfom}, qualitative translational consistency \cite{Lohani2016,opfvta}, as well as applicability to psychopharmacological profiling \cite{drlfom}.
Further,  opto-fMRI has facilitated the disambiguation of dopaminergic and non-dopaminergic Ventral Tegmental Area (VTA) signalling in reward circuitry \cite{Brocka2018}, and has uncovered significant deviations in functional dopaminergic VTA connectivity from what could linearly be inferred based on structural connectivity \cite{Lohani2016}.
A core advantage which renders such studies illuminating beyond simple SNR incrementation compared to chemogenetic or resting-state fMRI, is that it constitutes a qualitative leap towards the \textit{ceteris paribus}\footnote{i.e. “other things being held constant”, as commonly used to characterise the generalizability of statistical regularities \cite{Oulis2018}.} estimation of effective connectivity.
Being able to drive neuronal subpopulations directly, the causal functional effects --- rather than merely the correlative functional features --- of activity in a system can be clearly highlighted (cf. “effective connectivity” and “functional connectivity” \cite{Friston2011}).
Although the limited temporal resolution of current fMRI technology renders an elucidation of second and higher order downstream signal relay infeasible, opto-fMRI is currently able to map out neuronal-subpopulation-based signal propagation one step at a time.
Such step-wise functional projection profiles can further be conceptualized as phenotypical characteristics (i.e. “functional neurophenotypes” \cite{imstpc}) and be subject to experimental manipulation.

A particular feature in the stimulation and whole-brain assay of long-ranging projections, including but not limited to monoaminergic projections, consists in the ability to differentiate responses across subcellular compartments at macroscopic resolutions --- \textit{in vivo} and non-invasively, with respect to the measurement modality.
Given sufficiently high statistical contrast, combined with cell-type selective stimulation as achieved via opto-fMRI, neuronal compartments can be observed to show different response valences or kinetics.
This is represented by a graphical model in \cref{fig:cb}, and substantiated by experimental results which showcase somatic and post/synaptic voxels displaying either similar (\cref{fig:ofmri_experiments}a) or opposite (\cref{fig:ofmri_experiments}b) responses.
Hence, targeted activation of dopaminergic neurons in the VTA prompts post-synaptic excitation, while targeted activation of serotonergic neurons in the DR prompts post-synaptic inhibition.

Such localization is particularly relevant in the study of neuropsychiatric interventions, where targets may be differentially distributed across neurons (see receptor distributions in \cref{fig:cb}).
Correspondingly, proof of principle applications of opto-fMRI have been able to deliver cell-compartment level profiling of both acute \cite{Grandjean2019} and chronic antidepressant action, and have shown differential longitudinal profiles for somatic, cortical synaptic, and subcortical synaptic areas \cite{drlfom}.

On account of being neuronal subpopulation-selective as well as contingent on the induction of signal amplitudes sufficiently high to overcome the SNR constraints of fMRI, opto-fMRI commonly elicits neurotransmitter depletion.
This manifests itself in incremental response-amplitude reduction (i.e. biologically meaningful refractory effects), as seen in \cref{fig:ofmri_experiments}c.
Though extant literature commonly handles this feature as an ancillary nuisance process, the ubiquity of this phenomenon constitutes a representative example for an implicit assay of a neurophysiologically relevant process, which can be reexamined in refined analysis instantiations.
More generally, given the spatial standardization presupposed by whole-brain imaging, opto-fMRI characteristically produces manifold possibilities for quantitative data reuse and integration, as otherwise uncommon in the study of cell biological assays.


